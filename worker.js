chrome.runtime.onInstalled.addListener(
  async () => {
    const ruleIds = (await chrome.declarativeNetRequest.getDynamicRules()).map((x) => x.id);
    chrome.declarativeNetRequest.updateDynamicRules({ removeRuleIds: ruleIds });
    chrome.storage.sync.set({ id: 0 });
  },
);
