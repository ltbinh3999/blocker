function ruleRow(rule) {
  const url = document.createElement('div');
  url.textContent = rule.condition.urlFilter;

  const deleteButton = document.createElement('button');
  deleteButton.addEventListener('click', () => {
    chrome.declarativeNetRequest.updateDynamicRules({ removeRuleIds: [rule.id] });
    document.getElementById(`rule-${rule.id}`).remove();
  });
  deleteButton.textContent = 'X';

  const wrapper = document.createElement('div');
  wrapper.id = `rule-${rule.id}`;
  wrapper.append(url, deleteButton);

  return wrapper;
}

async function main() {
  const rules = (await chrome.declarativeNetRequest.getDynamicRules()).map((x) => ruleRow(x));
  document.getElementById('rules').append(...rules);

  document.getElementById('add').addEventListener('click', async () => {
    const { id } = await chrome.storage.sync.get('id');
    await chrome.storage.sync.set({ id: id + 1 });
    const rule = {
      id: id + 1,
      action: { type: 'block' },
      condition: {
        urlFilter: document.getElementById('url').value, resourceTypes: ['main_frame'],
      },
    };
    chrome.declarativeNetRequest.updateDynamicRules({
      addRules: [rule],
    });

    document.getElementById('rules').appendChild(ruleRow(rule));
    document.getElementById('url').value = '';
  });
}
main();
